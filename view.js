var userData = []

function submit() {
  let newData = {
    EmpName: document.getElementById('name').value,
    EmpCode: document.getElementById('role').value,
    Salary: document.getElementById('skills').value.match(/([^,]+)/g),
  }
  userData.push(newData)
  createUser(newData)
  console.log(newData)

  document.getElementById('name').value = ''
  document.getElementById('role').value = ''
  document.getElementById('skills').value = ''
}

async function createUser(data) {
  const response = await fetch(`http://localhost:5000/employees`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ user: data }),
  })
  console.log(response)
  return await response.json()
}

async function test() {
  userData = await getAllUsers()
  console.log(userData)
  document.getElementById('list').innerHTML = ''
  userData.forEach((user) => {
    let dat = user.name
    let datRole = user.role
    document.getElementById('list').innerHTML +=
      dat.toUpperCase() + '(' + datRole + ')' + '<br/>'
  })
}

async function getAllUsers() {
  try {
    const response = await fetch('http://localhost:5000/employees')
    return await response.json()
  } catch (error) {
    return []
  }
}

// document.getElementById('skills').value.match(/([^,]+)/g),
