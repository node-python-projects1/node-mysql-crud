const mysql = require('mysql')
const express = require('express')
var cors = require('cors')
var app = express()
app.use(cors())
app.use(express.json())

var mysqlConnect = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'Monica',
  database: 'employeedb',
  multipleStatements: true,
})

mysqlConnect.connect((err) => {
  if (err) {
    console.log(err)
  }
  console.log('Connection successfull !!')
})

//Get all employees data
app.get('/employees', (req, res) => {
  mysqlConnect.query('Select * from employee', (err, rows, fields) => {
    if (err) {
      console.log(err)
    } else {
      res.send(rows)
    }
  })
})

//Get an employee data using unique id
app.get('/employees/:id', (req, res) => {
  mysqlConnect.query(
    'Select * from employee where EmpId=?',
    [req.params.id],
    (err, rows, fields) => {
      if (err) {
        console.log(err)
      } else {
        res.send(rows)
      }
    },
  )
})

//deletes an employee record based on unique id
app.delete('/employees/:id', (req, res) => {
  mysqlConnect.query(
    'Delete from employee where EmpId=?',
    [req.params.id],
    (err, rows, fields) => {
      if (err) {
        console.log(err)
      } else {
        res.send('Deleted successfully')
      }
    },
  )
})

//inserts employee record
app.post('/employees', function (req, res, next) {
  // store all the user input data
  const userDetails = req.body.user

  // insert user data into users table
  var sql = 'INSERT INTO employee SET ?'
  mysqlConnect.query(sql, userDetails, function (err, data) {
    if (err) throw err
    console.log('User dat is inserted successfully ')
    res.json('Data Added successfully!!!')
  })
})

app.put('/employees/:id', function (req, res) {
  // store all the user input data
  var id = req.params.id
  var updateData = req.body
  var sql = `UPDATE employee SET ? WHERE EmpID= ?`
  mysqlConnect.query(sql, [updateData, id], function (err, data) {
    if (err) throw err
    console.log('User dat updated successfully ')
    res.json('Data Updated successfully')
  })
})

//updates an employee record based on unique id
// app.put('/employees/', (req, res) => {
//   var emp = req.body
//   var sql =
//     'SET @EmpID=?;SET @EmpName=?;SET @EmpCode=?;SET @Salary=?; \
//     CALL EmployeeAddOrEdit(@EmpID,@EmpName,@EmpCode,@Salary);'
//   mysqlConnect.query(
//     sql,
//     [emp.EmpID, emp.EmpName, emp.EmpCode, emp.Salary],
//     (err, rows, fields) => {
//       if (err) {
//         console.log(err)
//       } else {
//         res.send('Updated record')
//       }
//     },
//   )
// })

app.listen(5000, (err) => {
  console.log(err)
})
